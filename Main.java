import java.io.*;

class Main{
	public static void main( String[] args ){
		try{
			Hash tableA = new Hash( "TableA.txt" ); // tabela externa no loop 
			Hash tableB = new Hash( "TableB.txt" ); // tabela(interna no loop) com o índice hashing, 4 buckets
		
			Join.nestedLoopIndexedJoin( tableA, tableB	 );
		} catch( IOException e ){}
	}
}
