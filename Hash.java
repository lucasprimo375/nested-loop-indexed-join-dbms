import java.io.*;

public class Hash{
	String dataBase;
	int next;
	int round;
	int size;
	
	public Hash( String dataBaseName ){
		this.dataBase = dataBaseName;
		this.next = 0;
		this.round = 0;
		this.size = 4;
	}
	
	public String search( int value ) throws IOException {
		int bucket = value % this.size;
		
		int count = 0; 
		BufferedReader buffRead = new BufferedReader(new FileReader(this.dataBase));
   		String line = buffRead.readLine();
    	while (( line != null ) && ( count < bucket )) {
        	line = buffRead.readLine();
        	count++;
    	}
    	
    	return line;
	}
}
