import java.io.*;
import java.util.*;

public class Join{
	/*  Para cada linha da tabela externa, TableA, quebramos ela em registros (chave, valor).
		Para cada registro, fazemos uma pesquisa usando o índice da outra tabela e retornamos
		a linha correspondente. Depois, comparamos se chave dos registros é igual. Se sim,
		mostramos os dois registros. */
	public static ArrayList<Record> nestedLoopIndexedJoin( Hash tableA, Hash tableB ) throws IOException {
		ArrayList<Record> records = new ArrayList<Record>();
		int numAcessos = 0;
		System.out.println( "Valor Tabela A | Valor Tabela B" );
		
		BufferedReader buffReadA = new BufferedReader(new FileReader(tableA.dataBase));
		String lineA = buffReadA.readLine();
		while( lineA != null ){
			Record[] recordsA = breakLine( lineA ); // pegando os registros da linha
			int recordsASize = recordsA.length;
			
			for( int i=0; i<recordsASize; i++ ){
				String lineB = tableB.search( recordsA[i].key ); // fazendo uma busca no índice hashing pra cada registro
				numAcessos++;
				Record[] recordsB = breakLine( lineB );
				int recordsBSize = recordsB.length;
				
				for( int j=0; j<recordsBSize; j++ ){ // percorrendo o resultado da busca
					if( recordsA[i].key == recordsB[j].key ){ // se o valor dos atributos é igual, mostrar as tuplas da junção
						System.out.println( recordsA[i].value + " | " + recordsB[j].value );
					}
				}
 			}
			
			lineA = buffReadA.readLine();
		}
		System.out.println( "Número de acessos: " + numAcessos );
		
		return null;
	}
	
	public static Record[] breakLine( String line ) throws IOException {
		String[] records = line.split("#");
		
		Record[] actualRecords = new Record[records.length];
		
		int i = 0;
		for( String s : records ){
			String[] r = s.split(",");
			actualRecords[i] = new Record( Integer.parseInt(r[0]), r[1] );
			i++;
		}
		
		return actualRecords;
	}
}
